import Vue from "vue";
import Router from "vue-router";
import ReadMore from 'vue-read-more';
 
Vue.use(ReadMore);
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      redirect: "/home",
      component: () => import("@/view/layout/Layout"),
      children: [
        {
          path: "/home",
          name: "home",
          component: () => import("@/view/pages/Home.vue")
        },
        {
          path: "/store",
          name: "store",
          component: () => import("@/view/pages/Store.vue")
        },
        {
          path: "/cart",
          name: "cart",
          component: () => import("@/view/pages/Cart.vue")
        }
      ]
    },
    {
      path: "/",
      component: () => import("@/view/pages/auth/login_pages/Login-1"),
      children: [
        {
          name: "login",
          path: "/login",
          component: () => import("@/view/pages/auth/login_pages/Login-1")
        },
      ]
    },
    {
      path: "*",
      redirect: "/404"
    },
    {
      // the 404 route, when none of the above matches
      path: "/404",
      name: "404",
      component: () => import("@/view/pages/error/Error-1.vue")
    }
  ]
});
