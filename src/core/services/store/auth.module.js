import ApiService from "@/core/services/api.service";
import JwtService from "@/core/services/jwt.service";

import Services from "@/core/services/myapp/Services";
import Swal from "sweetalert2";

// action types
export const VERIFY_AUTH = "verifyAuth";
export const LOGIN = "login";
export const LOGOUT = "logout";
export const REGISTER = "register";
export const UPDATE_PASSWORD = "updatePassword";
export const UPDATE_USER = "updateUser";

// mutation types
export const PURGE_AUTH = "logOut";
export const SET_AUTH = "setUser";
export const SET_PASSWORD = "setPassword";
export const SET_ERROR = "setError";

const state = {
  errors: null,
  user: {},
  isAuthenticated: !!JwtService.getToken()
};

const getters = {
  currentUser(state) {
    return state.user;
  },
  isAuthenticated(state) {
    return state.isAuthenticated;
  }
};

const actions = {
  [LOGIN](context, credentials) {
    return new Promise(resolve => {
      ApiService.get(`people/1`, credentials)
        .then(({ data }) => {
          localStorage.setItem(
            "User_Name",
            credentials.username
          );
          localStorage.setItem(
            "User_Email",
            ""
          );
          localStorage.setItem(
            "User_Phone",
            ""
          );
          localStorage.setItem(
            "User_Address",
            ""
          );
          context.commit(SET_AUTH, data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.data.errors);
        });
    });
  },
  [LOGOUT](context) {
    localStorage.clear();
    context.commit(PURGE_AUTH);
  },
  [REGISTER](context, credentials) {
    return new Promise(resolve => {
      var mydata = {
        email: credentials.email,
        name: credentials.name,
        username: credentials.username,
        password: credentials.password
      };

      let contentType = `application/x-www-form-urlencoded`;

      const qs = require("qs");

      Services.PostData(
        ApiService,
        "users/register",
        qs.stringify(mydata),
        contentType,
        response => {
          resolve(response);
          if (response.data != null) {
            context.commit(SET_AUTH, response.data);
          } else {
            context.commit(SET_ERROR, []);
            Swal.fire({
              title: "",
              text: "Gagal Login",
              icon: "error",
              confirmButtonClass: "btn btn-secondary",
              heightAuto: true,
              timer: 1500
            });
          }
        },
        err => {
          context.commit(SET_ERROR, err);
        }
      );
    });
  },
  [VERIFY_AUTH](context) {
    if (JwtService.getToken()) {
      ApiService.setHeader();
      // ApiService.get("verify")
      //   .then(({ data }) => {
      //     context.commit(SET_AUTH, data);
      //   })
      //   .catch(({ response }) => {
      //     context.commit(SET_ERROR, response.data.errors);
      //   });
    } else {
      context.commit(PURGE_AUTH);
    }
  },
  [UPDATE_PASSWORD](context, payload) {
    const password = payload;

    return ApiService.put("password", password).then(({ data }) => {
      context.commit(SET_PASSWORD, data);
      return data;
    });
  },
  [UPDATE_USER](context, credentials) {
    return new Promise(resolve => {
      var mydata = {
        name: credentials.name,
        email: credentials.email,
        avatar: credentials.photos
      };

      var uid = credentials.uid;

      let contentType = `application/x-www-form-urlencoded`;

      const qs = require("qs");

      Services.UpdateData(
        ApiService,
        `users/${uid}`,
        qs.stringify(mydata),
        contentType,
        response => {
          resolve(response);
          if (response.data != null) {
            Swal.fire({
              title: "",
              text: "Berhasil Update",
              icon: "success",
              confirmButtonClass: "btn btn-secondary",
              heightAuto: true,
              timer: 1500
            });
            localStorage.deleteLocalStorage("User_Photo");
            localStorage.deleteLocalStorage("User_Email");
            localStorage.deleteLocalStorage("User_Fullname");
            localStorage.setItem(
              "User_Photo",
              response.data.user.avatar
            );
            localStorage.setItem(
              "User_Email",
              response.data.user.email
            );
            localStorage.setItem(
              "User_Fullname",
              response.data.user.name
            );
          } else {
            context.commit(SET_ERROR, []);
            Swal.fire({
              title: "",
              text: "Gagal Login",
              icon: "error",
              confirmButtonClass: "btn btn-secondary",
              heightAuto: true,
              timer: 1500
            });
          }
        },
        err => {
          context.commit(SET_ERROR, err);
        }
      );
    });
  }
};

const mutations = {
  [SET_ERROR](state, error) {
    state.errors = error;
  },
  [SET_AUTH](state, user) {
    state.isAuthenticated = true;
    state.user = user;
    state.errors = {};
    JwtService.saveToken(state.user.token);
  },
  [SET_PASSWORD](state, password) {
    state.user.password = password;
  },
  [PURGE_AUTH](state) {
    state.isAuthenticated = false;
    state.user = {};
    state.errors = {};
    JwtService.destroyToken();
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};
